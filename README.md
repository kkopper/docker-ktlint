# docker-ktlint

A Docker image based on [`eclipse-temurin:21-jre`](https://hub.docker.com/_/eclipse-temurin/) with [ktlint](https://ktlint.github.io) installed.
Up to version `1.2.1` of ktlint, [`openjdk:11-jre-slim`](https://hub.docker.com/_/openjdk/) was used as base image.

The resulting Docker image is automatically deployed to both [Docker Hub](https://hub.docker.com/r/kkopper/ktlint/) and this projects [container registry](https://gitlab.com/kkopper/ktlint/container_registry).

All created Docker tags correspond to tags in this repository, with `latest` always being the most recent commit on the master branch.
