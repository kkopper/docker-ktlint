#! /bin/sh --

# Creates a JUnit XML report based on GitLab CI environment variables.
# https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html
# https://www.ibm.com/docs/en/developer-for-zos/16.0?topic=formats-junit-xml-format

# Capture current time to compute test duration later.
TIMESTAMP=$(date -u +%s.%N)

# Ensure CI environment
if [ -z "$CI" ] || [ "$CI" != 'true' ]
then
    echo 'Not in a CI environment.' >&2
    exit 1
fi

# The BusyBox version of date does not parse ISO dates, but offers the possibility to define
# strptime compatible format strings, which is not defined by POSIX and hence unavailable in
# other shells. The correct command is selected by checking the help output of date.
if date --help 2>&1 | grep -q BusyBox
then
    JOB_STARTED_AT=$(date -ud "$CI_JOB_STARTED_AT" -D '%Y-%m-%dT%TZ' +%s.%N)
else
    JOB_STARTED_AT=$(date -ud "$CI_JOB_STARTED_AT" +%s.%N)
fi

# Compute test duration as difference between current time and the start time of the CI job.
TIME_ELAPSED=$( echo "scale=3; ($TIMESTAMP - $JOB_STARTED_AT) / 1" | bc )

# "Count" numbers of failed or errornous tests based on CI job status.
[ "$CI_JOB_STATUS" = 'failed' ] ; NUM_FAILURES=$(( 1 - $? ))
[ "$CI_JOB_STATUS" = 'canceled' ] ; NUM_ERRORS=$(( 1 - $? ))

# Create XML report
echo '<?xml version="1.0" encoding="UTF-8" ?>'

echo "<testsuites id=\"$CI_PIPELINE_IID\" name=\"$CI_PROJECT_NAME - $CI_JOB_NAME\" tests=\"1\" failures=\"$NUM_FAILURES\" errors=\"$NUM_ERRORS\" time=\"$TIME_ELAPSED\" timestamp=\"$CI_PIPELINE_CREATED_AT\">"

echo "<testsuite id=\"$CI_JOB_NAME_SLUG\" name=\"$CI_JOB_NAME\" tests=\"1\" failures=\"$NUM_FAILURES\" errors=\"$NUM_ERRORS\" time=\"$TIME_ELAPSED\" timestamp=\"$CI_JOB_STARTED_AT\">"

echo "<testcase id=\"$CI_JOB_NAME_SLUG\" name=\"$CI_JOB_NAME\" time=\"$TIME_ELAPSED\">"

if [ "$CI_JOB_STATUS" = 'failed' ]
then
    echo '<failure message="Invoking the image failed." type="ERROR">'
    echo 'Take a look at the job output for more details.'
    echo '</failure>'
fi

echo '</testcase>'

echo '</testsuite>'

echo '</testsuites>'
